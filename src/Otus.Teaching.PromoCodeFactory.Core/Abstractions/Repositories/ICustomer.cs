﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomer:IRepository<Customer>
    {
        List<Customer> GetCustomersByPreference(Preference preference); //TODO вынести в сервисный слой
    }
}
