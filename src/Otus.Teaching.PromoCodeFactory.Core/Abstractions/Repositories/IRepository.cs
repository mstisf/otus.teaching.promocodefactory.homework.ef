﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        IEnumerable<T> GetAll();
        Task<T> GetByIdAsync(Guid id);
        T GetById(Guid id);
        Task<T> CreateAsync(T item);
        Task CreateRangeAsync(ICollection<T> items);
        void CreateRange(ICollection<T> items);
        T Create(T item);
        void Update(T item);
        bool Delete(Guid id);
        bool DeleteRange(ICollection<T> items);
        void SaveChanges();
        Task SaveChangesAsync();

    }
}
