﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Exeptions
{
    public class ElementNotExistsExeption : Exception
    {
        public ElementNotExistsExeption() : base("Элемента не существует!") { }
    }
}
