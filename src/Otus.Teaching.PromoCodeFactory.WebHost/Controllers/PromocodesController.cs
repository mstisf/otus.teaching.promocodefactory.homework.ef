﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IPromocode _promocodeRepository;
        private readonly IPreference _preferenceRepository;
        private readonly ICustomer _customerRepository;
        private readonly ICustomerPreference _customerPreferenceRepository;
        public PromocodesController(
            IPromocode promocodeRepository,
            IPreference preferenceRepository,
            ICustomer customerRepository,
            ICustomerPreference customerPreferenceRepository
            )
        {
            _promocodeRepository = promocodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            //TODO: Получить все промокоды 
            var promocodes = (await _promocodeRepository.GetAllAsync()).Select(p => new PromoCodeShortResponse
            {
                BeginDate = p.BeginDate.ToShortDateString(),
                Code = p.Code,
                EndDate = p.EndDate.ToShortDateString(),
                Id = p.Id,
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            });
            return Ok(promocodes);
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            //TODO: Создать промокод и выдать его клиентам с указанным предпочтением
            var preference = await _preferenceRepository.GetByIdAsync(Guid.Parse(request.Preference));
            if (preference == null) return BadRequest("Нет редпочтений с guid " + request.Preference);

            var customers = _customerRepository.GetCustomersByPreference(preference);
            
            foreach (var customer in customers)
            {

                var promocod = await _promocodeRepository.CreateAsync(new PromoCode()
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Preference = preference,
                    Id = Guid.NewGuid(),
                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),
                });
                customer.PromoCodes.Add(promocod); //TODO проверить не выдан ли ранее с тем же Code 
                _customerRepository.Update(customer);
                
            }
            await _promocodeRepository.SaveChangesAsync();
           // await _customerRepository.SaveChangesAsync();
            return Ok();
        }
    }
}