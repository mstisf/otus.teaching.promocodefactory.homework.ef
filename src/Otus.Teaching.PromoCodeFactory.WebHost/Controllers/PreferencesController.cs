﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    
    public class PreferencesController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        public PreferencesController(IRepository<Preference> preferenceRepository) 
        { 
            _preferenceRepository = preferenceRepository;
        }
        /// <summary>
        /// Получение всех предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetPreferensesAsync()
        {
            var prefs = await _preferenceRepository.GetAllAsync();
            if (prefs == null || prefs.Count()==0)
            {
                return NoContent();
            }
            return Ok(prefs.Select(p=>new PreferenceResponse { Id=p.Id,Name=p.Name}));
        }
    }
}
