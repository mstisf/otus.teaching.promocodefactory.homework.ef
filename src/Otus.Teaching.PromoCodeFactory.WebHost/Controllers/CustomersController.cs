﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {

        //private readonly IRepository<Customer> _customerRepository;
        //private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        //private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly ICustomer _customerRepository;
        private readonly ICustomerPreference _customerPreferenceRepository;
        private readonly IPromocode _promocodeRepository;

        public CustomersController(
            ICustomer customerRepository,
            ICustomerPreference customerPreferenceRepository,
            IPromocode promocodeRepository)
        {
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _promocodeRepository = promocodeRepository;
        }
        /// <summary>
        ///  Получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            //TODO: Добавить получение списка клиентов
            var customers = await _customerRepository.GetAllAsync();
            var customersResponse = customers.Select(c => new CustomerResponse
            {
                Email = c.Email,
                FirstName = c.FirstName,
                Id = c.Id,
                LastName = c.LastName,
                PromoCodes = c.PromoCodes.Select(p => new PromoCodeShortResponse
                {
                    BeginDate = p.BeginDate.ToShortDateString(),
                    Code = p.Code,
                    EndDate = p.EndDate.ToShortDateString(),
                    Id = p.Id,
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo
                }).ToList(),
                Preferences = c.CustomerPreferences.Select(p => new PreferenceResponse
                {
                    //CustomerPreferences = p.Preference.CustomerPreferences,
                    Id = p.PreferenceId,
                    Name = p.Preference?.Name,

                }).ToList(),
            });
            if (customersResponse.Count() == 0) return NoContent();
            return Ok(customersResponse);
        }
        /// <summary>
        /// Получение клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            //TODO: Добавить получение клиента вместе с выданными ему промомкодами
            if (id == default(Guid)) return BadRequest("Пустой id");

            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null) return NoContent();

            var customerResponse = new CustomerResponse
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                Id = customer.Id,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes.Select(p => new PromoCodeShortResponse
                {
                    BeginDate = p.BeginDate.ToShortDateString(),
                    Code = p.Code,
                    EndDate = p.EndDate.ToShortDateString(),
                    Id = p.Id,
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo
                }).ToList(),
                Preferences = customer.CustomerPreferences.Select(p => new PreferenceResponse
                {
                    Id = p.PreferenceId,
                    Name = p.Preference?.Name,

                }).ToList(),
            };
            return Ok(customerResponse);
        }
        /// <summary>
        /// Cоздание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            //TODO: Добавить создание нового клиента вместе с его предпочтениями
            if (request == null) return BadRequest("Пустой объект для создания клиента");
            Guid id = Guid.NewGuid();
            Customer item = new Customer()
            {
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Id = id

            };

            var customer = await _customerRepository.CreateAsync(item: item);

            var CustomerPreferences = request.PreferenceIds.Select(cp => new CustomerPreference
            {
                CustomerId = id,
                PreferenceId = cp,
                Id = Guid.NewGuid()

            }).ToList();
            await _customerPreferenceRepository.CreateRangeAsync(CustomerPreferences);

            await _customerRepository.SaveChangesAsync();
            //await _customerPreferenceRepository.SaveChangesAsync();
            return Ok(new CustomerResponse
            {
                Email = customer.Email,
                FirstName = customer.FirstName,
                Id = customer.Id,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes?.Select(p => new PromoCodeShortResponse
                {
                    BeginDate = p.BeginDate.ToShortDateString(),
                    Code = p.Code,
                    EndDate = p.EndDate.ToShortDateString(),
                    Id = p.Id,
                    PartnerName = p.PartnerName,
                    ServiceInfo = p.ServiceInfo
                })?.ToList(),
                Preferences = CustomerPreferences.Select(p => new PreferenceResponse
                {

                    Id = p.PreferenceId,
                    Name = "<Неопределено>" // что бы не делать лишних запросов - выводим только Id

                }).ToList()
            });
        }
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            //TODO: Обновить данные клиента вместе с его предпочтениями
            var element = await _customerRepository.GetByIdAsync(id);

            if (element == null)
            {
                return BadRequest("Не найден элемент с id = " + id);
            }


            element.Email = request.Email;
            element.FirstName = request.FirstName;
            element.LastName = request.LastName;
            element.Id = id;


            _customerRepository.Update(element);
            await _customerRepository.SaveChangesAsync();

            //if (_customerPreferenceRepository.DeleteRange(element.CustomerPreferences))
            //{
            //    _customerPreferenceRepository.CreateRange(request.PreferenceIds.Select(p => new CustomerPreference
            //    {
            //        CustomerId = element.Id,
            //        Id = Guid.NewGuid(),
            //        PreferenceId = p
            //    }).ToList());
            //}

            var oldIds = element.CustomerPreferences.Select(ep => ep.PreferenceId).ToList();
            var newIds = request.PreferenceIds;
            //var needAdd = newIds.Except(oldIds);
            var needAdd = newIds.Where(rp => !oldIds.Contains(rp));
            var needRemove = element.CustomerPreferences.Where(cp => !newIds.Contains(cp.PreferenceId));

            if (_customerPreferenceRepository.DeleteRange(needRemove.ToList()))
            {
                _customerPreferenceRepository.CreateRange(needAdd.Select(p => new CustomerPreference
                {
                    CustomerId = element.Id,
                    Id = Guid.NewGuid(),
                    PreferenceId = p
                }).ToList());
                await _customerPreferenceRepository.SaveChangesAsync();
            }
            else
            {
                return StatusCode(500, "Ошибка удаления предпочтений");
            }
            return Ok();

        }
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            //TODO: Удаление клиента вместе с выданными ему промокодами
            var element = await _customerRepository.GetByIdAsync(id);

            if (!_customerPreferenceRepository.DeleteRange(element.CustomerPreferences))
            {
                return StatusCode(500, "Ошибка удаления предпочтений");
            }

            await _customerPreferenceRepository.SaveChangesAsync();

            if (!_promocodeRepository.DeleteRange(element.PromoCodes))
            {
                return StatusCode(500, "Ошибка удаления промокодов");
            }
            await _promocodeRepository.SaveChangesAsync();

            if (!_customerRepository.Delete(id))
            {
                return StatusCode(500, "Ошибка удаления клиента");

            }
            await _customerRepository.SaveChangesAsync();
            return Ok();


        }
    }
}