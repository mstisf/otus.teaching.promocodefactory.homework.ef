using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using System.ComponentModel;
using Microsoft.AspNetCore.Cors.Infrastructure;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddDbContext<ApplicationDBContext>(c =>
            {
                var connectionString = $"{Configuration["ConnectionString"]}";
                c.UseSqlite(connectionString);
            });
            services.AddControllers();
            services.AddScoped<IDbInitializer, DbInitializer>();

            //try
            //{
            //    var Context = serviceProvider.GetRequiredService<ApplicationDBContext>();

            //    await DataSeed.SeedAsync(Context, logger);

            //}
            //catch (Exception ex)
            //{
            //    logger.LogError(ex, "An error occurred seeding the DB.");
            //}


            //services.AddScoped(typeof(IRepository<Employee>), (x) =>
            //    new InMemoryRepository<Employee>(FakeDataFactory.Employees));
            //services.AddScoped(typeof(IRepository<Role>), (x) =>
            //    new InMemoryRepository<Role>(FakeDataFactory.Roles));
            //services.AddScoped(typeof(IRepository<Preference>), (x) =>
            //    new InMemoryRepository<Preference>(FakeDataFactory.Preferences));
            //services.AddScoped(typeof(IRepository<Customer>), (x) =>
            //    new InMemoryRepository<Customer>(FakeDataFactory.Customers));

            services.AddScoped(typeof(IRepository<>), typeof(EFRepository<>));

            services.AddScoped<ICustomer, CustomerRepository>();
            services.AddScoped<IPreference, PreferenceRepository>();
            services.AddScoped<ICustomerPreference, CustomerPreferenceRepository>();
            services.AddScoped<IPromocode, PromoCodeRepository>();

            services.Configure<IServiceScopeFactory>((sp) =>
            {
                using (var scope = sp.CreateScope())
                {
                    var scopeFactory = scope.ServiceProvider.GetRequiredService<IServiceScopeFactory>();
                    var dbInitializer = scope.ServiceProvider.GetService<IDbInitializer>();
                    dbInitializer.Initialize();
                    dbInitializer.SeedData();
                }
            });
            //using (var scope = scopeFactory.CreateScope())
            //{
            //    var dbInitializer = scope.ServiceProvider.GetService<IDbInitializer>();
            //    dbInitializer.Initialize();
            //    dbInitializer.SeedData();
            //}


            services.AddOpenApiDocument(options =>
            {
                options.Title = "PromoCode Factory API Doc";
                options.Version = "1.0";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceScopeFactory serviceScopeFactory)
        {


            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            using (var scope = serviceScopeFactory.CreateScope())
            {
                var dbInitializer = scope.ServiceProvider.GetService<IDbInitializer>();
                dbInitializer.Initialize();
                dbInitializer.SeedData();
            }

        }
    }

    //public interface IDbInitializer
    //{
    //    /// <summary>
    //    /// Applies any pending migrations for the context to the database.
    //    /// Will create the database if it does not already exist.
    //    /// </summary>
    //    void Initialize();

    //    /// <summary>
    //    /// Adds some default values to the Db
    //    /// </summary>
    //    Task SeedData();
    //}

    //public class DbInitializer : IDbInitializer
    //{
    //    private readonly IServiceScopeFactory _scopeFactory;
    //    private readonly ILogger _logger;

    //    public DbInitializer(IServiceScopeFactory scopeFactory, ILogger<DbInitializer> logger)
    //    {
    //        this._scopeFactory = scopeFactory;
    //        this._logger = logger;
    //    }

    //    public void Initialize()
    //    {
    //        using (var serviceScope = _scopeFactory.CreateScope())
    //        {
    //            using (var context = serviceScope.ServiceProvider.GetService<ApplicationDBContext>())
    //            {
    //                context.Database.EnsureDeleted();
    //                context.Database.EnsureCreated();
    //                //context.Database.Migrate();
    //            }
    //        }
    //    }

    //    public async Task SeedData()
    //    {
    //        using (var serviceScope = _scopeFactory.CreateScope())
    //        {
    //            using (var context = serviceScope.ServiceProvider.GetService<ApplicationDBContext>())
    //            {                 

    //                 await DataSeed.SeedAsync(context, _logger);
    //            }
    //        }
    //    }
    //}
}