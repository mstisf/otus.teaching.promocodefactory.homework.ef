﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EF
{
    public class ApplicationDBContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
        : base(options)
        {
 
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });

            modelBuilder.Entity<CustomerPreference>()
                .HasOne<Customer>(u => u.Customer)
                .WithMany(c => c.CustomerPreferences)
                .HasForeignKey(cp => cp.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
               .HasOne<Preference>(u => u.Preference)
               .WithMany(c => c.CustomerPreferences)
               .HasForeignKey(cp => cp.PreferenceId);

            modelBuilder.Entity<Employee>()
                .HasOne(u => u.Role)
                .WithMany(c => c.Employees)
                .IsRequired();


            modelBuilder.Entity<PromoCode>()
                .HasOne(u => u.Customer)
                .WithMany(c => c.PromoCodes);
                



            //modelBuilder.Entity<Course>().HasIndex(c=>c.Name);

            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);

            modelBuilder.Entity<Role>().Property(c =>  c.Name ).HasMaxLength(100);
            modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(300);
            modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(100);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            optionsBuilder
               .UseLazyLoadingProxies();
               

        }

    }
}
