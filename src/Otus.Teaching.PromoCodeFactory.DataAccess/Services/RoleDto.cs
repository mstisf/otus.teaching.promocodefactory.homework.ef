﻿using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services
{
    public class RoleDto
        
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}