﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Services
{
    public class EmployeeDto
        
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public RoleDto Role { get; set; }

        public int AppliedPromocodesCount { get; set; }
    }
}