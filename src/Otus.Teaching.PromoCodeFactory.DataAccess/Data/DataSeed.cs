﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataSeed
    {
        public static async Task SeedAsync(ApplicationDBContext Context,
            ILogger<DbInitializer> logger,
            int retry = 0)
        {
            var retryForAvailability = retry;
            try
            {
                logger.LogInformation("begin seed");

                if (!await Context.Employees.AnyAsync())
                {
                    await Context.Employees.AddRangeAsync(
                        FakeDataFactory.Employees);

                    await Context.SaveChangesAsync();
                }

                if (!await Context.Roles.AnyAsync())
                {
                    await Context.Roles.AddRangeAsync(
                        FakeDataFactory.Roles);

                    await Context.SaveChangesAsync();
                }

                if (!await Context.Preferences.AnyAsync())
                {
                    await Context.Preferences.AddRangeAsync(
                        FakeDataFactory.Preferences);

                    await Context.SaveChangesAsync();
                }
                if (!await Context.Customers.AnyAsync())
                {
                    await Context.Customers.AddRangeAsync(
                        FakeDataFactory.Customers);

                    await Context.SaveChangesAsync();
                }
                logger.LogInformation("end seed");
            }
            catch (Exception ex)
            {
                if (retryForAvailability >= 10) throw;

                retryForAvailability++;

                logger.LogError(ex.Message);
                await SeedAsync(Context, logger, retryForAvailability);
                throw;
            }
        }


    }

}

