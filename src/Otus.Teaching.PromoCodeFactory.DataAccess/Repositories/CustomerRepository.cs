﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EFRepository<Customer>, ICustomer
    {
        private readonly ApplicationDBContext dBContext;
        public CustomerRepository(ApplicationDBContext context) : base(context)
        {
            dBContext = context;
        }

        public List<Customer> GetCustomersByPreference(Preference preference) //TODO вынести в сервисный слой
        {
            return dBContext.Customers.Where(c => c.CustomerPreferences.
            Any(cp => cp.PreferenceId == preference.Id))?.ToList();
        }
    }
}
