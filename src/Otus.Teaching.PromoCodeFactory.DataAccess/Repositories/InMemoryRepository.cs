﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Exeptions;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T item)
        {

            object locker = new object();
            lock (locker)
            {
                if (item.Id == Guid.Empty)
                {
                    item.Id = Guid.NewGuid();
                }
                var finded = GetByIdAsync(item.Id);
                if (finded.Result != null)
                {
                    throw new DoubleElementException();
                }
                var newData = Data.ToList();
                newData.Add(item);
                Data = newData;
            }
            return Task.FromResult(item);
        }

        public Task Update(T item)
        {

            object locker = new object();
            lock (locker)
            {
                var tupleIndex = Data.Select((employee, index) => (index, employee)).FirstOrDefault(t => t.employee.Id == item.Id);
                if (tupleIndex.employee == null)
                {
                    throw new ElementNotExistsExeption();
                }
                var newData = Data.ToList();
                newData[tupleIndex.index] = item;

                Data = newData;

            }
            return Task.CompletedTask;
        }

        public Task Delete(Guid id)
        {

            object locker = new object();
            lock (locker)
            {
                var newData = Data.Where(t => t.Id != id);
                if (newData.Count() == Data.Count())
                {
                    throw new ElementNotExistsExeption();
                }


                Data = newData;

            }
            return Task.CompletedTask;
        }

        public IEnumerable<T> GetAll()
        {
            throw new NotImplementedException();
        }

        public T GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public Task CreateRangeAsync(ICollection<T> items)
        {
            throw new NotImplementedException();
        }

        public void CreateRange(ICollection<T> items)
        {
            throw new NotImplementedException();
        }

        public T Create(T item)
        {
            throw new NotImplementedException();
        }

        void IRepository<T>.Update(T item)
        {
            throw new NotImplementedException();
        }

        bool IRepository<T>.Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public Task SaveChangesAsync()
        {
            throw new NotImplementedException();
        }

        public bool DeleteRange(ICollection<T> items)
        {
            throw new NotImplementedException();
        }
    }
}