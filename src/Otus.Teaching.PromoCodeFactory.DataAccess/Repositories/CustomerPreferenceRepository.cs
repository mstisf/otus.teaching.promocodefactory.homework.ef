﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerPreferenceRepository : EFRepository<CustomerPreference>, ICustomerPreference
    {
        public CustomerPreferenceRepository(ApplicationDBContext context) : base(context)
        {
        }
    }
}
