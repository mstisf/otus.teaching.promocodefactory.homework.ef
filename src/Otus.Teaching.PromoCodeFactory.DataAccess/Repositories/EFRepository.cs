﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EFRepository<T> : IRepository<T> where T : BaseEntity
    {

        private ApplicationDBContext _db;
        private readonly DbSet<T> _dbSet;
        public EFRepository(ApplicationDBContext context)
        {
            this._db = context;
            _dbSet = _db.Set<T>();
        }

        public virtual T Create(T item)
        {
            return _dbSet.Add(item).Entity;
        }

        public virtual async Task<T> CreateAsync(T item)
        {
            return (await _dbSet.AddAsync(item)).Entity;

        }

        public virtual void CreateRange(ICollection<T> items)
        {
            if (items == null) return;
            _dbSet.AddRange(items);
        }

        public virtual async Task CreateRangeAsync(ICollection<T> items)
        {
            if (items == null || items.Count == 0) return;
            await _dbSet.AddRangeAsync(items);
        }

        public virtual bool Delete(Guid id)
        {
            var obj = _dbSet.Find(id);
            if (obj == null) return false;
            _dbSet.Remove(obj);
            return true;
        }
 

        public virtual IEnumerable<T> GetAll()
        {
            return _dbSet;
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public virtual T GetById(Guid id)
        {
            if (id == null) return default(T);
            return _dbSet.Find(id);
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            if (id == null) return default(T);
            return await _dbSet.FindAsync(id);
        }

        public virtual void SaveChanges()
        {
            _db.SaveChanges();
        }

        public virtual async Task SaveChangesAsync()
        {
             await _db.SaveChangesAsync();
        }

        public void Update(T item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }

        public bool DeleteRange(ICollection<T> items)
        {
            if(items == null) return false;
            _dbSet.RemoveRange(items);
            return true;

        }
    }
}
